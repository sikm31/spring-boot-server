package com.example.demo.controller;

import com.example.demo.model.Application;
import com.example.demo.model.User;
import com.example.demo.service.ApplicationService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/adops")
@PreAuthorize("hasAuthority('ADOPS')")
public class AdopsController {

	@Autowired
	private UserService userService;

	@Autowired
	private ApplicationService applicationService;

	@RequestMapping(method = RequestMethod.GET)
	public String index(Principal principal) {
		return "Hello " + principal.getName();
	}

	@RequestMapping(value = "/register/publisher",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createUserPublisher(@RequestBody User user) {
		if (userService.findByEmail(user.getEmail()) != null) {
			return new ResponseEntity<>(new User(), HttpStatus.OK);
		}
		user.setRole(user.getRole().PUBLISHER);
		userService.createUser(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/publisher/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateUserPublisher(@PathVariable("id") long id, @RequestBody User user) {
		user.setRole(user.getRole().PUBLISHER);
		userService.editUser(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUserPublisher(@PathVariable("id") Integer id) {

		User user = userService.getUser(id);
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		if (!"PUBLISHER".equals(user.getRole().name())) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		userService.deleteUser(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/register/app",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createApplication(@RequestBody Application application) {
		Application applicationOld = applicationService.getApplication(application.getId());
		if (applicationOld.getId().equals(application.getId())) {
			return new ResponseEntity<>(new Application(), HttpStatus.OK);
		}

		applicationService.createApplication(application);
		return new ResponseEntity<>(application, HttpStatus.OK);
	}

	@RequestMapping(value = "/app/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateApplication(@PathVariable("id") long id, @RequestBody Application application) {
		applicationService.editApplication(application);
		return new ResponseEntity<>(application, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteApplication(@PathVariable("id") Integer id) {
		Application application = applicationService.getApplication(id);
		if (application == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		applicationService.deleteApplication(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
