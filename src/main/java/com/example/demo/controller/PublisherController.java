package com.example.demo.controller;

import com.example.demo.model.Application;
import com.example.demo.service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/publisher")
@PreAuthorize("hasAuthority('PUBLISHER')")
public class PublisherController {


	@Autowired
	private ApplicationService applicationService;

	@RequestMapping(method = RequestMethod.GET)
	public String index(Principal principal) {
		return "Hello " + principal.getName();
	}

	@RequestMapping(value = "/register/app",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createApplication(@RequestBody Application application) {
		Application applicationOld = applicationService.getApplication(application.getId());
		if (applicationOld.getId().equals(application.getId())) {
			return new ResponseEntity<>(new Application(), HttpStatus.OK);
		}
		applicationService.createApplication(application);
		return new ResponseEntity<>(application, HttpStatus.OK);
	}

	@RequestMapping(value = "/app/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateApplication(@PathVariable("id") long id, @RequestBody Application application) {
		applicationService.editApplication(application);
		return new ResponseEntity<>(application, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteApplication(@PathVariable("id") Integer id) {
		Application application = applicationService.getApplication(id);
		if (application == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		applicationService.deleteApplication(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
