package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/admin")
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminController {

	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET)
	public String index(Principal principal) {
		return "Hello " + principal.getName();
	}

	@RequestMapping(value = "/register/publisher",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createUserPublisher(@RequestBody User user) {
		if (userService.findByEmail(user.getEmail()) != null) {
			return new ResponseEntity<>(new User(), HttpStatus.OK);
		}
		user.setRole(user.getRole().PUBLISHER);
		userService.createUser(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/register/adops",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createUserAdops(@RequestBody User user) {
		if (userService.findByEmail(user.getEmail()) != null) {
			return new ResponseEntity<>(new User(), HttpStatus.OK);
		}
		user.setRole(user.getRole().ADOPS);
		userService.createUser(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ResponseEntity<?> getUsers() {
		return new ResponseEntity<>(userService.allUsers(), HttpStatus.OK);
	}

	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public User getOne(@PathVariable(value = "id") Integer id) {
		return userService.getUser(id);
	}

	@RequestMapping(value = "/publisher/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateUserPublisher(@PathVariable("id") long id, @RequestBody User user) {
		user.setRole(user.getRole().PUBLISHER);
		userService.editUser(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/adops/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateUserAdops(@PathVariable("id") long id, @RequestBody User user) {
		user.setRole(user.getRole().ADOPS);
		userService.editUser(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUserAdops(@PathVariable("id") Integer id) {

		User user = userService.getUser(id);
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		if (!"ADOPS".equals(user.getRole().name())) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		userService.deleteUser(id);
		return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUserPublisher(@PathVariable("id") Integer id) {

		User user = userService.getUser(id);
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		if (!"PUBLISHER".equals(user.getRole().name())) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		userService.deleteUser(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}


}
