package com.example.demo.model;

public class Constants {

	public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5*60*60;
	public static final String SIGNING_KEY = "yuri123r";
	public static final String TOKEN_PREFIX = "Yuri ";
	public static final String HEADER_STRING = "Authorization";
}
