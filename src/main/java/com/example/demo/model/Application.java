package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "app")
public class Application {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	@Enumerated(EnumType.STRING)
	private AppType type;

	@ElementCollection(fetch = FetchType.EAGER,targetClass = ContentType.class)
	@Enumerated(EnumType.STRING)
	private Set<ContentType> contentTypes;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private User user;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AppType getType() {
		return type;
	}

	public void setType(AppType type) {
		this.type = type;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<ContentType> getContentTypes() {
		return contentTypes;
	}

	public void setContentTypes(Set<ContentType> contentTypes) {
		this.contentTypes = contentTypes;
	}
}
