package com.example.demo.service;

import com.example.demo.model.User;

import java.util.List;

public interface UserService {

	User findByEmail(String email);
	List<User> allUsers();
	void createUser(User user);
	User findByUsername(String username);
	User getUser(Integer id);
	void deleteUser(Integer id);
	void editUser(User user);

}
