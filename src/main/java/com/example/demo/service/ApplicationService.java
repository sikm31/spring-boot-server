package com.example.demo.service;

import com.example.demo.model.Application;

import java.util.List;

public interface ApplicationService {

	List<Application> getAllApplication();
	void createApplication(Application application);
	Application getApplication(Integer id);
	void deleteApplication(Integer id);
	void editApplication(Application application);
}
