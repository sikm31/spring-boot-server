package com.example.demo.service;

import com.example.demo.dao.ApplicationDao;
import com.example.demo.model.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApplicationServiceImpl implements ApplicationService {

	@Autowired
	ApplicationDao applicationDao;

	@Override
	public List<Application> getAllApplication() {
		return applicationDao.getAllApplication();
	}

	@Override
	public void createApplication(Application application) {
		applicationDao.createApplication(application);
	}

	@Override
	public Application getApplication(Integer id) {
		return applicationDao.getApplication(id);
	}

	@Override
	public void deleteApplication(Integer id) {
		applicationDao.deleteApplication(id);
	}

	@Override
	public void editApplication(Application application) {
		applicationDao.editApplication(application);
	}
}
