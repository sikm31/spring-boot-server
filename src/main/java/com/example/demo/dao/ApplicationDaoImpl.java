package com.example.demo.dao;

import com.example.demo.model.Application;
import com.example.demo.model.User;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
@Transactional
public class ApplicationDaoImpl implements ApplicationDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Application> getAllApplication() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Application.class);
		return (List<Application>)criteria.list();
	}

	@Override
	public void createApplication(Application application) {
		Session session = sessionFactory.getCurrentSession();
		session.save(application);
	}

	@Override
	public Application getApplication(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Application.class);
		criteria.add(Restrictions.eq("id",id));
		return (Application) criteria.uniqueResult();
	}

	@Override
	public void deleteApplication(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Application application = session.load(Application.class, id);
		session.delete(application);
	}

	@Override
	public void editApplication(Application applicationOld) {
		Session session = sessionFactory.getCurrentSession();
		Application application = getApplication(applicationOld.getId());
		session.save(application);
	}
}
