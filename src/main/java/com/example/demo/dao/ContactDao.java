package com.example.demo.dao;

import com.example.demo.model.Contact;
import org.hibernate.ScrollableResults;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContactDao {
    List<Contact> getAll();
    List<Contact> getSearch(String searchName);
    List<Contact> getSearchCondition(String searchName);
}
