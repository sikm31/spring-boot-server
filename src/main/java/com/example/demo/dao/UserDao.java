package com.example.demo.dao;

import com.example.demo.model.User;

import java.util.List;

public interface UserDao {

	User findByEmail(String email);
	void createUser(User user);
	User getUser(Integer id);
	List<User> allUsers();
	User findByUsername(String username);
	void deleteUser(Integer id);
	void editUser(User user);

}
