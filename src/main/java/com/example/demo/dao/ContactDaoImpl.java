package com.example.demo.dao;

import com.example.demo.model.Contact;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Repository
@Transactional
public class ContactDaoImpl implements ContactDao {


	@Autowired
	private SessionFactory sessionFactory;


	@Override
	public List<Contact> getAll() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Contact.class);
		ScrollableResults results = session.createCriteria(Contact.class).setFetchSize(100)
				.setCacheMode(CacheMode.GET).scroll(ScrollMode.FORWARD_ONLY);
		return criteria.list();

	}

	@Override
	public List<Contact> getSearch(String searchName) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Contact.class)
				.setFetchSize(100)
				.setCacheable(true)
				.setCacheMode(CacheMode.GET);
		criteria.add(Restrictions.not(Restrictions.sqlRestriction("name REGEXP '"+searchName+"'")));
		return criteria.list();

	}

	@Override
	public List<Contact> getSearchCondition(String searchName) {
		List<Contact> listSearch = new ArrayList<>();
		if (!searchName.isEmpty()) {
			Session session = sessionFactory.getCurrentSession();
			ScrollableResults result = session.createCriteria(Contact.class)
					.setFetchSize(100)
					.setCacheable(true)
					.setCacheMode(CacheMode.GET)
					.scroll(ScrollMode.FORWARD_ONLY);
			while (result.next()) {
				Contact contact = (Contact) result.get(0);
				boolean isSameTemplate = Pattern.matches(searchName, contact.getName());
				if (!isSameTemplate) {
					listSearch.add(contact);
				}
			}
		}
		return listSearch;
	}
}
