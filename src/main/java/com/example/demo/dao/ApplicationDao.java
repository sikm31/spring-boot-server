package com.example.demo.dao;

import com.example.demo.model.Application;
import com.example.demo.model.User;

import java.util.List;

public interface ApplicationDao {

	List<Application> getAllApplication();
	void createApplication(Application application);
	Application getApplication(Integer id);
	void deleteApplication(Integer id);
	void editApplication(Application application);

}
