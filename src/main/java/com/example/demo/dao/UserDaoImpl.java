package com.example.demo.dao;

import com.example.demo.model.Contact;
import com.example.demo.model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User findByEmail(String email) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("email",email));
		return (User) criteria.uniqueResult();
	}

	@Override
	public void createUser(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.save(user);
	}

	@Override
	public User getUser(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("id",id));
		return (User) criteria.uniqueResult();
	}


	@Override
	public List<User> allUsers() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(User.class);
		List<User> userList = (List<User>)criteria.list();
		return userList;
	}

	@Override
	public User findByUsername(String username) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("name",username));
		return (User) criteria.uniqueResult();
	}

	@Override
	public void deleteUser(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		User user = session.load(User.class, id);
		session.delete(user);
	}

	@Override
	public void editUser(User userOld) {
		Session session = sessionFactory.getCurrentSession();
		User user = getUser(userOld.getId());
		user.setName(userOld.getName());
		user.setPassword(userOld.getPassword());
		user.setRole(userOld.getRole());
		user.setEmail(userOld.getEmail());
		session.save(user);
	}

}
