//package com.example.demo.configuration;
//
//
//import com.example.demo.service.CustomUserDetailsService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
//
//import javax.sql.DataSource;
//
//@Configuration
//@EnableWebSecurity
//public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
//
////	@Autowired
////	private BCryptPasswordEncoder bCryptPasswordEncoder;
//
//	@Autowired
//	private DataSource dataSource;
//
//
//	@Autowired
//	private CustomUserDetailsService userDetailsService;
//
//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.userDetailsService(userDetailsService);//.passwordEncoder(bCryptPasswordEncoder);
//	}
//
////	@Bean
////	public WebMvcConfigurer corsConfigurer() {
////		return new WebMvcConfigurerAdapter() {
////			@Override
////			public void addCorsMappings(CorsRegistry registry) {
////				registry.addMapping("/**").allowedOrigins("http://localhost:4200");
////
////			}
////		};
////	}
//
//	@Override
//	public void configure(WebSecurity web) throws Exception {
//		super.configure(web);
//	}
//
////	@Override
////	protected void configure(HttpSecurity http) throws Exception {
////		http.cors().and()
////				// starts authorizing configurations
////				.authorizeRequests()
////				// ignoring the guest's urls "
////				.antMatchers("/register", "/login", "/logout").permitAll()
////				.antMatchers(HttpMethod.GET, "/admin/home/**").hasRole("ADMIN")
////				.antMatchers(HttpMethod.POST, "/admin/home/**").hasRole("ADMIN")
////				// authenticate all remaining URLS
////				.anyRequest().fullyAuthenticated().and()
////				/* "/logout" will log the user out by invalidating the HTTP Session,
////				 * cleaning up any {link rememberMe()} authentication that was configured, */
////				.logout()
////				.permitAll()
////				.logoutRequestMatcher(new AntPathRequestMatcher("/logout", "POST"))
////				.and()
////				// enabling the basic authentication
////				.httpBasic().and()
////				// configuring the session on the server
////				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).and()
////				// disabling the CSRF - Cross Site Request Forgery
////				.csrf().disable();
////	}
//
////	@Autowired
////	CustomSecurityFilter customSecurityFilter;
//
////	@Value("${spring.queries.users-query}")
////	private String usersQuery;
////
////	@Value("${spring.queries.roles-query}")
////	private String rolesQuery;
//
//
////	@Autowired
////	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
////		auth.jdbcAuthentication().dataSource(dataSource)
////				.usersByUsernameQuery("select username,password,enabled from user where username = ?")
////				.authoritiesByUsernameQuery("select username,role as authority from user where username = ?");
////
////	}
//
//
////	@Override
////	protected void configure(AuthenticationManagerBuilder auth)
////			throws Exception {
//////		auth
//////				.inMemoryAuthentication()
//////				.withUser("adops")
//////				.password("adops")
//////				.roles("ADOPS")
//////				.and()
//////				.withUser("publisher")
//////				.password("publisher")
//////				.roles("PUBLISHER")
//////				.and()
//////				.withUser("admin")
//////				.password("admin")
//////				.roles("ADOPS", "PUBLISHER", "ADMIN");
////	}
//
////	@Override
////	protected void configure(AuthenticationManagerBuilder auth)
////			throws Exception {
//////		auth.
//////				jdbcAuthentication()
//////				.usersByUsernameQuery(usersQuery)
//////				.authoritiesByUsernameQuery(rolesQuery)
//////				.dataSource(dataSource)
//////				.passwordEncoder(bCryptPasswordEncoder);
////		auth.jdbcAuthentication().dataSource(dataSource)
////				.usersByUsernameQuery("select name,password from user where name = ?")
////				.authoritiesByUsernameQuery("select name,role as authority from user where name = ?")
////				.passwordEncoder(bCryptPasswordEncoder);
////	}
//
//
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//
//		http.
//				authorizeRequests()
//				.antMatchers("/").permitAll()
//				.antMatchers("/login").permitAll()
//				.antMatchers("/registration").permitAll()
//				.antMatchers("/admin/home/**").hasAuthority("ADMIN")
//				.antMatchers("/publisher/home/**").hasAuthority("PUBLISHER")
//				.antMatchers("/adops/home/**").hasAuthority("ADOPS").anyRequest()
//				.authenticated().and().csrf().disable().formLogin()
//				.loginPage("/login").failureUrl("/login?error=true")
//				//.defaultSuccessUrl("/admin/home")
//				.usernameParameter("username")
//				.passwordParameter("password")
//				.and().logout()
//				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//				.logoutSuccessUrl("/").and().exceptionHandling()
//				.accessDeniedPage("/access-denied");
//	}
//
////	@Override
////	public void configure(WebSecurity web) throws Exception {
////		web
////				.ignoring()
////				.antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
////	}
//
//
////	@Override
////	protected void configure(HttpSecurity http) throws Exception {
////		http
////				.csrf().disable()
////				.authorizeRequests()
////				.antMatchers("/login").permitAll()
////				.antMatchers("/admin/home/**").authenticated()
////				.antMatchers(HttpMethod.GET, "/admin/home/**").hasRole("ADMIN")
////				.antMatchers(HttpMethod.POST, "/admin/home/**").hasRole("ADMIN")
////				.antMatchers("/apos").authenticated()
////				.and().addFilterBefore(customSecurityFilter, UsernamePasswordAuthenticationFilter.class);
////
////
////	}
//
//
////	@Override
////	protected void configure(HttpSecurity http) throws Exception {
////		http
////				.authorizeRequests()
////				.anyRequest()
////				.authenticated()
////				.and()
////				.httpBasic();
////	}
//
//}
